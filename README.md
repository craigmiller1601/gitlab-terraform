# Craigmiller160 GitLab Settings (Terraform)

A terraform project that controls all the settings for my GitLab repositories.

## Setting Up Secrets

Create a file `.env` with the following contents:

```shell
export TF_VAR_gitlab_token = "ABCDEFG"
export TF_VAR_onepassword_token = "HIJKLMNOP"
```

Make sure you run `source .env` prior to running terraform commands so you have access to these secrets.

## How to Add Projects to Terraform

Simply run `update-project-list.sh` and the list of projects will be auto-updated. Then run `terraform apply` to bring them under management.

## How to See Runner Token

```bash
cd gitlab
terraform output -raw runner_token
# Ignore the trailing % sign
```