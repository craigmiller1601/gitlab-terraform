locals {
  mr_commit_template =  <<EOF
Merge branch '%%{source_branch}' into '%%{target_branch}'

MR: %%{reference} - %%{title}

%%{description}
EOF
}

import {
  for_each = {
    for project in local.projects : project.name => project
  }

  to = gitlab_project.craigmiller160_project[each.key]
  id = each.value.id
}

resource "gitlab_project" "craigmiller160_project" {
  for_each = {
    for project in local.projects : project.name => project
  }

  namespace_id = gitlab_group.craigmiller160.id
  name = each.key

  visibility_level = "public"
  request_access_enabled = false
  issues_access_level = "private"
  repository_access_level = "enabled"
  merge_requests_access_level = "private"
  lfs_enabled = false
  builds_access_level = "private"
  container_registry_access_level = "private"
  analytics_access_level = "private"
  packages_enabled = false
  pages_access_level = "enabled"
  monitor_access_level = "private"
  environments_access_level = "private"
  feature_flags_access_level = "private"
  infrastructure_access_level = "private"
  emails_enabled = true
  merge_pipelines_enabled = false
  squash_option = "always"
  releases_access_level = "enabled"
  ci_default_git_depth = 1
  shared_runners_enabled = false
  group_runners_enabled = true
  merge_commit_template = local.mr_commit_template
  squash_commit_template = local.mr_commit_template

  lifecycle {
    prevent_destroy = true
  }
}

# import {
#   for_each = {
#     for project in local.projects : project.name => project
#   }
#
#   to = gitlab_branch_protection.main_branch_protection[each.key]
#   id = "${each.value.id}:main"
# }

# resource "gitlab_branch_protection" "main_branch_protection" {
#   for_each = {
#     for project in local.projects : project.name => project
#   }
#
#   branch  = "main"
#   project = each.key
#
#   push_access_level = "maintainer"
#   merge_access_level = "maintainer"
#   unprotect_access_level = "maintainer"
# }