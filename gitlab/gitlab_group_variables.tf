data "onepassword_item" "nexus_credentials" {
  vault = "k6xneqw7nf5f2fm4azxhbdrcji"
  uuid = "7g4pldy24s33nyfnjajscc4ehi"
}

data "onepassword_item" "argocd_credentials" {
  vault = "k6xneqw7nf5f2fm4azxhbdrcji"
  uuid = "c7nohalqa5ouwdljiyncsz7rie"
}

data "onepassword_item" "kubernetes_admin_token" {
  vault = "k6xneqw7nf5f2fm4azxhbdrcji"
  uuid = "bvnavb2khrqwcob3btxlme3vfq"
}

data "onepassword_item" "kubernetes_ca_data" {
  vault = "k6xneqw7nf5f2fm4azxhbdrcji"
  uuid = "myditdu3duomx2zee6fbdmieli"
}

data "onepassword_item" "onepass_access_token" {
  vault = "k6xneqw7nf5f2fm4azxhbdrcji"
  uuid = "cd7vlowzqkpq7oud2s4yok4unq"
}

data "onepassword_item" "craigmiller160_gpg_key" {
  vault = "k6xneqw7nf5f2fm4azxhbdrcji"
  uuid = "n5vcxkcr6n2t5il6uofekhrlwy"
}

data "onepassword_item" "craigmiller160_maven_central_creds" {
  vault = "k6xneqw7nf5f2fm4azxhbdrcji"
  uuid = "c64zwaieuvb7kgx6edcryqdxzq"
}

locals {
  group_vars = {
    nexus_username_key = "NEXUS_USER"
    nexus_password_key = "NEXUS_PASSWORD"
    nexus_npm_token_key = "NEXUS_NPM_TOKEN"
    gitlab_access_token_key = "ACCESS_TOKEN"
    argocd_username_key = "ARGOCD_USERNAME"
    argocd_password_key = "ARGOCD_PASSWORD"
    k8s_user_token_key = "K8S_USER_TOKEN"
    k8s_ca_data_key = "K8S_CA_DATA"
    onepassword_token_key = "ONEPASSWORD_TOKEN"
    craigmiller160_gpg_username_key = "CRAIGMILLER160_GPG_KEY_USERNAME"
    craigmiller160_gpg_password_key = "CRAIGMILLER160_GPG_KEY_PASSWORD"
    craigmiller160_gpg_public_key = "CRAIGMILLER160_GPG_PUBLIC_KEY_BASE64"
    craigmiller160_gpg_private_key = "CRAIGMILLER160_GPG_PRIVATE_KEY_BASE64"
    craigmiller160_gpg_id_key = "CRAIGMILLER160_GPG_KEY_ID"
    maven_central_username_key = "MAVEN_CENTRAL_USERNAME"
    maven_central_password_key = "MAVEN_CENTRAL_PASSWORD"
  }
}

import {
  id = "${local.craigmiller160_group_id}:${local.group_vars.nexus_username_key}:*"
  to = gitlab_group_variable.nexus_username
}

resource "gitlab_group_variable" "nexus_username" {
  group = gitlab_group.craigmiller160.id
  key   = local.group_vars.nexus_username_key
  value = sensitive(data.onepassword_item.nexus_credentials.username)
  masked = local.variables_masked
}

import {
  id = "${local.craigmiller160_group_id}:${local.group_vars.nexus_password_key}:*"
  to = gitlab_group_variable.nexus_password
}

resource "gitlab_group_variable" "nexus_password" {
  group = gitlab_group.craigmiller160.id
  key   = local.group_vars.nexus_password_key
  value = sensitive(data.onepassword_item.nexus_credentials.password)
  masked = local.variables_masked
}

import {
  id = "${local.craigmiller160_group_id}:${local.group_vars.nexus_npm_token_key}:*"
  to = gitlab_group_variable.nexus_npm_token
}

resource "gitlab_group_variable" "nexus_npm_token" {
  group = gitlab_group.craigmiller160.id
  key   = local.group_vars.nexus_npm_token_key
  value = sensitive(data.onepassword_item.nexus_credentials.section[0].field[1].value)
  masked = local.variables_masked
}

import {
  id = "${local.craigmiller160_group_id}:${local.group_vars.gitlab_access_token_key}:*"
  to = gitlab_group_variable.access_token
}

resource "gitlab_group_variable" "access_token" {
  group = gitlab_group.craigmiller160.id
  key   = local.group_vars.gitlab_access_token_key
  value = sensitive(var.gitlab_token)
  masked = local.variables_masked
}

import {
  id = "${local.craigmiller160_group_id}:${local.group_vars.argocd_username_key}:*"
  to = gitlab_group_variable.argocd_username
}

resource "gitlab_group_variable" "argocd_username" {
  group = gitlab_group.craigmiller160.id
  key   = local.group_vars.argocd_username_key
  value = sensitive(data.onepassword_item.argocd_credentials.username)
  # Must be "admin", and that is too short for the masking algorithm for... reasons?
  masked = false
}

import {
  id = "${local.craigmiller160_group_id}:${local.group_vars.argocd_password_key}:*"
  to = gitlab_group_variable.argocd_password
}

resource "gitlab_group_variable" "argocd_password" {
  group = gitlab_group.craigmiller160.id
  key   = local.group_vars.argocd_password_key
  value = sensitive(data.onepassword_item.argocd_credentials.password)
  masked = local.variables_masked
}

import {
  id = "${local.craigmiller160_group_id}:${local.group_vars.k8s_user_token_key}:*"
  to = gitlab_group_variable.k8s_user_token
}

resource "gitlab_group_variable" "k8s_user_token" {
  group = gitlab_group.craigmiller160.id
  key   = local.group_vars.k8s_user_token_key
  value = sensitive(data.onepassword_item.kubernetes_admin_token.password)
  masked = local.variables_masked
}

import {
  id = "${local.craigmiller160_group_id}:${local.group_vars.k8s_ca_data_key}:*"
  to = gitlab_group_variable.k8s_ca_data
}

resource "gitlab_group_variable" "k8s_ca_data" {
  group = gitlab_group.craigmiller160.id
  key   = local.group_vars.k8s_ca_data_key
  value = sensitive(data.onepassword_item.kubernetes_ca_data.password)
  masked = local.variables_masked
}

import {
  id = "${local.craigmiller160_group_id}:${local.group_vars.onepassword_token_key}:*"
  to = gitlab_group_variable.onepass_access_token
}

resource "gitlab_group_variable" "onepass_access_token" {
  key     = local.group_vars.onepassword_token_key
  group = gitlab_group.craigmiller160.id
  value   = sensitive(data.onepassword_item.onepass_access_token.credential)
  masked = local.variables_masked
}

import {
  id = "${local.craigmiller160_group_id}:${local.group_vars.craigmiller160_gpg_username_key}:*"
  to = gitlab_group_variable.craigmiller160_gpg_key_username
}

resource "gitlab_group_variable" "craigmiller160_gpg_key_username" {
  group = gitlab_group.craigmiller160.id
  key   = local.group_vars.craigmiller160_gpg_username_key
  value = data.onepassword_item.craigmiller160_gpg_key.username
  masked = local.variables_masked
}

import {
  id = "${local.craigmiller160_group_id}:${local.group_vars.craigmiller160_gpg_password_key}:*"
  to = gitlab_group_variable.craigmiller160_gpg_key_password
}

resource "gitlab_group_variable" "craigmiller160_gpg_key_password" {
  group = gitlab_group.craigmiller160.id
  key   = local.group_vars.craigmiller160_gpg_password_key
  value = sensitive(data.onepassword_item.craigmiller160_gpg_key.password)
  masked = local.variables_masked
}

import {
  id = "${local.craigmiller160_group_id}:${local.group_vars.craigmiller160_gpg_public_key}:*"
  to = gitlab_group_variable.craigmiller160_gpg_public_key
}

resource "gitlab_group_variable" "craigmiller160_gpg_public_key" {
  group = gitlab_group.craigmiller160.id
  key   = local.group_vars.craigmiller160_gpg_public_key
  value = sensitive(data.onepassword_item.craigmiller160_gpg_key.section[0].file[0].content_base64)
  masked = local.variables_masked
}

import {
  id = "${local.craigmiller160_group_id}:${local.group_vars.craigmiller160_gpg_private_key}:*"
  to = gitlab_group_variable.craigmiller160_gpg_private_key
}

resource "gitlab_group_variable" "craigmiller160_gpg_private_key" {
  group = gitlab_group.craigmiller160.id
  key   = local.group_vars.craigmiller160_gpg_private_key
  value = sensitive(data.onepassword_item.craigmiller160_gpg_key.section[0].file[1].content_base64)
  masked = local.variables_masked
}

import {
  id = "${local.craigmiller160_group_id}:${local.group_vars.craigmiller160_gpg_id_key}:*"
  to = gitlab_group_variable.craigmiller160_gpg_key_id
}

resource "gitlab_group_variable" "craigmiller160_gpg_key_id" {
  group = gitlab_group.craigmiller160.id
  key   = local.group_vars.craigmiller160_gpg_id_key
  value = sensitive(data.onepassword_item.craigmiller160_gpg_key.section[1].field[0].value)
  masked = local.variables_masked
}

import {
  id = "${local.craigmiller160_group_id}:${local.group_vars.maven_central_username_key}:*"
  to = gitlab_group_variable.maven_central_username
}

resource "gitlab_group_variable" "maven_central_username" {
  group = gitlab_group.craigmiller160.id
  key   = local.group_vars.maven_central_username_key
  value = sensitive(data.onepassword_item.craigmiller160_maven_central_creds.username)
  masked = local.variables_masked
}

import {
  id = "${local.craigmiller160_group_id}:${local.group_vars.maven_central_password_key}:*"
  to = gitlab_group_variable.maven_central_password
}

resource "gitlab_group_variable" "maven_central_password" {
  group = gitlab_group.craigmiller160.id
  key   = local.group_vars.maven_central_password_key
  value = sensitive(data.onepassword_item.craigmiller160_maven_central_creds.password)
  masked = local.variables_masked
}