locals {
  craigmiller160_group_id = "87802809"
}

import {
  to = gitlab_group.craigmiller160
  id = local.craigmiller160_group_id
}

resource "gitlab_group" "craigmiller160" {
  name     = "craigmiller160"
  path     = "craigmiller1601"

  visibility_level = "public"
  lfs_enabled = false
  membership_lock = true
  request_access_enabled = false
  emails_enabled = true
  prevent_forking_outside_group = false
  project_creation_level = "maintainer"
  require_two_factor_authentication = true
  shared_runners_setting = "disabled_and_unoverridable"

  lifecycle {
    prevent_destroy = true
  }
}