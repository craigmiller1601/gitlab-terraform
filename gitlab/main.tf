terraform {
  required_providers {
    gitlab = {
      source = "gitlabhq/gitlab"
      version = "17.6.1"
    }

    onepassword = {
      source = "1Password/onepassword"
      version = "2.1.2"
    }
  }

  backend "kubernetes" {
    secret_suffix = "gitlab-state"
    namespace = "gitlab"
    config_path   = "~/.kube/config"
  }
}

provider "gitlab" {
  token = var.gitlab_token
  base_url = "https://gitlab.com/api/v4/"
}

provider "onepassword" {
  url = "https://infra.craigmiller160.us/onepassword"
  token = var.onepassword_token
}

locals {
  projects = jsondecode(file("${path.module}/project_list.json"))
  variables_masked = true
}