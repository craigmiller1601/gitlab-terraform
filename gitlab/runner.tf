import {
  id = "37551334"
  to = gitlab_user_runner.craigmiller160_k8s_runner
}

resource "gitlab_user_runner" "craigmiller160_k8s_runner" {
  description = "Craigmiller160 Kubernetes Executor"
  runner_type        = "group_type"
  group_id = gitlab_group.craigmiller160.id
  maximum_timeout = 600
  untagged = true
}

# To see this, run `cd gitlab && terraform output runner_token
output "runner_token" {
  sensitive = true
  value = gitlab_user_runner.craigmiller160_k8s_runner.token
}