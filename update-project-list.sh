#!/usr/bin/env bash

set -euo pipefail

GITLAB_DIR="./gitlab"
PROJECT_LIST_FILE="$GITLAB_DIR/project_list.json"
GITLAB_GROUP_ID=87802809
GITLAB_API_URL="https://gitlab.com/api/v4"
source .env

generate_repo_list() {
  echo "Generating project list file"
  curl \
    -s \
    --header "PRIVATE-TOKEN: $TF_VAR_gitlab_token" \
    "$GITLAB_API_URL/groups/$GITLAB_GROUP_ID/projects?per_page=10000" | \
    jq '[.[] | {name, id}]' \
    > "$PROJECT_LIST_FILE"
}

generate_repo_list